package de.ppluss.memoryviewer;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MemoryCommandExecutor implements CommandExecutor {

    protected final MemoryViewer plugin;

    public MemoryCommandExecutor(MemoryViewer plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(strings.length == 1 && strings[0].toLowerCase().equals("reloadconfig")) {
            plugin.initConfig();
            commandSender.sendMessage("Config reloaded!");
            return true;
        }

        long maximum = Runtime.getRuntime().maxMemory();
        long free = Runtime.getRuntime().freeMemory();
        long total = Runtime.getRuntime().totalMemory();

        long used = total - free;

        long usage = (used * 100) / maximum;
        long allocated = (total * 100) / maximum;

        long mbUsed = used / 1000000;
        long mbMaximum = maximum / 1000000;
        long mbAllocated = total / 1000000;

        commandSender.sendMessage("Java Memory: " + usage + "% " + mbUsed + "/" + mbMaximum + "MB");
        commandSender.sendMessage("Java Allocated: " + allocated + "% " + mbAllocated + "MB");

        if(plugin.checkSystemRAM && System.getProperty("os.name").contains("Linux")) {
            try {
                Process process = Runtime.getRuntime().exec("free -m");
                process.waitFor(); // TODO: Could freeze the main server thread (?)
                BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                reader.readLine(); // Skip the first line
                String line = reader.readLine();
                reader.close();

                // Try to parse the command output
                String[] splited = line.split(" ");
                ArrayList<String> parts = new ArrayList<>(Arrays.asList(splited));
                parts.removeAll(Collections.singletonList(""));

                int linuxUsed = Integer.parseInt(parts.get(2));
                int linuxTotal = Integer.parseInt(parts.get(1));

                int linuxUsage = (linuxUsed * 100) / linuxTotal;

                commandSender.sendMessage("Linux System Memory: " + linuxUsage + "% " + linuxUsed + "/" + linuxTotal + "MB" );
            } catch (IOException | InterruptedException e) {
                if(!plugin.suppressErrors) {
                    commandSender.sendMessage("Unable to get Linux system memory information; Stacktrace in console");
                    e.printStackTrace();
                } else {
                    commandSender.sendMessage("Unable to get Linux system memory information; Errors are being suppressed");
                }
            }
        }

        return true;
    }

}
