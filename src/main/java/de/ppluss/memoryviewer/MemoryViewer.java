package de.ppluss.memoryviewer;

import org.bstats.bukkit.Metrics;
import org.bukkit.plugin.java.JavaPlugin;

public class MemoryViewer extends JavaPlugin {

    public boolean checkSystemRAM, suppressErrors;

    @Override
    public void onEnable() {
        super.onEnable();

        initConfig();

        MemoryCommandExecutor memCmdExecutor = new MemoryCommandExecutor(this);
        this.getCommand("memory").setExecutor(memCmdExecutor);

        int pluginId = 6892;
        Metrics metrics = new Metrics(this, pluginId);
    }

    public void initConfig() {
        this.reloadConfig();
        this.saveDefaultConfig();

        this.checkSystemRAM = this.getConfig().getBoolean("check-system-ram");
        this.suppressErrors = this.getConfig().getBoolean("suppress-errors");
    }

}
